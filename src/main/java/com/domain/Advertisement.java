package com.domain;

import com.deserializer.LocalDateDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.serializer.AdvertisementSerializer;
import com.serializer.LocalDateSerializer;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;

@Entity
@JsonSerialize(using = AdvertisementSerializer.class)
public class Advertisement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Version
    private int version;

    @Column(name = "name_ad")
    @Pattern(regexp = "[a-zA-Z ]{2,50}")
    @NotNull
    private String name;

    @Column(name = "date_publish")
    @PastOrPresent
    @NotNull
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate datePublish;

    @Column(name = "description_ad")
    @Pattern(regexp = ".{5,50}")
    @NotNull
    private String descriptionAd;

    @Min(0)
    private int price;

    @ManyToOne
    @JoinColumn(name = "rubric_id_fk")
    @NotNull
    private Rubric rubric;

    @ManyToOne
    @JoinColumn(name = "advert_author_id_fk")
    @NotNull
    private Author author;

    public Advertisement() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDatePublish() {
        return datePublish;
    }

    public void setDatePublish(LocalDate datePublish) {
        this.datePublish = datePublish;
    }

    public String getDescriptionAd() {
        return descriptionAd;
    }

    public void setDescriptionAd(String descriptionAd) {
        this.descriptionAd = descriptionAd;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Rubric getRubric() {
        return rubric;
    }

    public void setRubric(Rubric rubric) {
        this.rubric = rubric;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Advertisement(String name, String descriptionAd, int price, Author author, Rubric rubric, LocalDate datePublish) {
        this.name = name;
        this.datePublish = datePublish;
        this.descriptionAd = descriptionAd;
        this.price = price;
        this.author = author;
        this.rubric = rubric;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Advertisement{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", datePublish=" + datePublish +
                ", descriptionAd='" + descriptionAd + '\'' +
                ", price=" + price +
                ", rubric=" + rubric.getRubricName() +
                ", author=" + author.getNameOfAuthor() +
                '}';
    }
}
