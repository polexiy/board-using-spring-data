package com.service;

import com.dao.AdvertisementDAO;
import com.domain.Advertisement;
import com.repository.AdvertisementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.awt.print.Pageable;
import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
public class AdvertisementService implements AdvertisementDAO {

    @Autowired
    private AdvertisementRepository repository;

    @Override
    public void save(Advertisement advertisement) {

        repository.save(advertisement);
    }

    @Override
    public void delete(List<Integer> ids) {

        repository.deleteAllByIdIn(ids);
    }

    @Override
    public Advertisement getById(int id) {

        return repository.findById(id).get();
    }

    @Override
    public List<Advertisement> showAllByDate(LocalDate date) {

        return repository.findAllByDatePublish(date);
    }

    @Override
    public List<Advertisement> getAllAdvertisements(int page, int size) {

        PageRequest pageRequest = PageRequest.of(page, size);

        return repository.findAll(pageRequest).getContent();
    }

    public void deleteAllByAuthorId(int authorId) {

        repository.deleteAllByAuthorId(authorId);
    }
}
