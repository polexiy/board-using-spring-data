package com.service;

import com.dao.AuthorDAO;
import com.domain.Advertisement;
import com.domain.Author;
import com.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.IntStream;

@Service
@Transactional
public class AuthorService implements AuthorDAO {

    @Autowired
    private AuthorRepository repository;

    @Override
    public Author getAuthorById(int id) {

        return repository.findById(id);
    }

    @Override
    public void save(Author author) {

        repository.save(author);
    }

    @Override
    public void delete(int id) {

        repository.deleteById(id);
    }

    @Override
    public List<Author> getAll() {

        return repository.findAll();
    }

    @Override
    public void deleteAllTheAdverts(int id) {

        repository.findById(id).getAdvertisements().clear();

        System.out.println();
    }
}
