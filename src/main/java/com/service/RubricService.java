package com.service;

import com.dao.RubricDAO;
import com.dto.RubricDTO;
import com.domain.Rubric;
import com.repository.RubricRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class RubricService implements RubricDAO {

    @Autowired
    private RubricRepository repository;

    @Override
    public void save(Rubric rubric) {

        repository.save(rubric);
    }

    @Override
    public List<RubricDTO> getAllRubrics() {

        List<RubricDTO> rubricsDTO = repository.findAll().stream().

                map(rubric -> new RubricDTO(rubric.getId(), rubric.getRubricName())).collect(Collectors.toList());

        return rubricsDTO;
    }

    @Override
    public List<Rubric> getRubricsById(String[] ids) {

        List<Integer> listIds = Arrays.asList(ids).stream().map(id -> Integer.parseInt(id)).collect(Collectors.toList());

        return (List<Rubric>) repository.findAllByIdIn(listIds);
    }

    @Override
    public void deleteById(int id) {

        repository.deleteById(id);
    }
}
