package com.controller;

import com.domain.Advertisement;
import com.service.AdvertisementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("advertisement")
public class AdvertisementController {

    @Autowired
    private AdvertisementService service;

    @PostMapping("/save")
    public void save(@RequestBody Advertisement advertisement) {

        service.save(advertisement);
    }

    @PutMapping("/update")
    public void update(@RequestBody Advertisement advertisement) {

        service.save(advertisement);
    }

    @DeleteMapping("/delete/{advert_ids}")
    public void delete(@PathVariable("advert_ids") String[] advertIds) {

        List<Integer> ids = Arrays.asList(advertIds).stream().map(id -> Integer.parseInt(id)).collect(Collectors.toList());

        service.delete(ids);
    }

    @GetMapping("/get_by_date/{date}")
    public List<Advertisement> showAllByDate(@PathVariable String date) {

        return service.showAllByDate(LocalDate.parse(date));
    }

    @GetMapping("/get_all/{page}/{size}")
    public List<Advertisement> getAll(@PathVariable("page") int page, @PathVariable("size") int size) {

        return service.getAllAdvertisements(page, size);
    }

    @GetMapping("/get_by_id/{id}")
    public Advertisement getById(@PathVariable("id") int id){

        return service.getById(id);
    }

    @DeleteMapping("/delete_by_author_id/{authorId}")
    public void deleteByAuthorId(@PathVariable("authorId") int authorId){

        service.deleteAllByAuthorId(authorId);
    }
}
