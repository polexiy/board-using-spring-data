package com.controller;

import com.dto.RubricDTO;
import com.domain.Rubric;
import com.service.RubricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("rubric")
public class RubricController {

    @Autowired
    private RubricService service;

    @PostMapping("/save")
    public void save(@RequestBody Rubric rubric) {

        service.save(rubric);
    }

    @GetMapping("/get_all_rubrics")
    public List<RubricDTO> getAllRubrics() {

        return service.getAllRubrics();
    }

    @GetMapping("/get/{rubric_id}/")
    public List<Rubric> getRubricById(@PathVariable("rubric_id") String [] ids) {

        return service.getRubricsById(ids);
    }

    @DeleteMapping("/remove/{rubric_id}")
    public void deleteById(@PathVariable("rubric_id") int rubricId) {

        service.deleteById(rubricId);
    }
}
