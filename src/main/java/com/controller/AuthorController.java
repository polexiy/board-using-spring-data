package com.controller;

import com.domain.Author;
import com.service.AuthorService;
import org.hibernate.annotations.BatchSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("author")
public class AuthorController {

    @Autowired
    private AuthorService service;

    @GetMapping("/get/{author_id}")
    public Author getById(@PathVariable("author_id") int id){

        Author authorById = service.getAuthorById(id);

        return authorById;
    }

    @PostMapping("/save")
    public void saveAuthor(@RequestBody Author author){

        service.save(author);
    }

    @PutMapping("/update")
    public void update(@RequestBody Author author) {

        service.save(author);
    }

    @DeleteMapping("/delete/{author_id}")
    public void delete(@PathVariable("author_id") int authorId){

        service.delete(authorId);
    }

    @DeleteMapping("/delete_all_adverts/{id}")
    public void deleteAllAdvertisements(@PathVariable("id") int id){

        service.deleteAllTheAdverts(id);
    }

    @GetMapping("/getAll")
    public List<Author> getAll(){

        return service.getAll();
    }
}
