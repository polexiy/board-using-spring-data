package com.dao;

import com.domain.Advertisement;

import java.time.LocalDate;
import java.util.List;

public interface AdvertisementDAO {

    void save(Advertisement advertisement);

    void delete(List<Integer> ids);

    Advertisement getById(int id);

    List<Advertisement> getAllAdvertisements(int page, int size);

    List<Advertisement> showAllByDate(LocalDate date);
}
