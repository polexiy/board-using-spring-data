package com.dao;

import com.domain.Rubric;
import com.dto.RubricDTO;

import java.util.List;

public interface RubricDAO {

    void save(Rubric rubric);

    List<Rubric> getRubricsById(String [] ids);

    void deleteById(int id);

    List<RubricDTO> getAllRubrics();
}
