package com.dao;

import com.domain.Author;

import java.util.List;

public interface AuthorDAO {

    void save(Author author);

    List<Author> getAll();

    Author getAuthorById(int id);

    void delete(int id);

    void deleteAllTheAdverts(int id);
}
