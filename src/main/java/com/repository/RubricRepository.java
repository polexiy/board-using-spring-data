package com.repository;

import com.domain.Rubric;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RubricRepository extends JpaRepository<Rubric, Integer> {

    @EntityGraph(attributePaths = {"advertisements"})
    Rubric findAllByIdIn(List<Integer> id);
}
