package com.repository;

import com.domain.Author;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Integer> {


    @EntityGraph(attributePaths = {"advertisements"})
    Author findById(int id);
}
