package com.repository;

import com.domain.Advertisement;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface AdvertisementRepository extends JpaRepository<Advertisement, Integer> {

    void deleteAllByIdIn(List<Integer> ids);

    List<Advertisement> findAllByDatePublish(LocalDate date);

    @Modifying
    @Query("UPDATE Advertisement a SET" +
            " a.name = :name" +
            " WHERE a.id = :id")
    void update(@Param("name") String name, @Param("id") int id);

    void deleteAllByAuthorId(int authorId);
}
